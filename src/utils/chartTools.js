const filterDataBySOCstock = (features, filteredParamName) => {
  var tempResult = {};
  let small = 0;
  let middle = 0;
  let large = 0;
  let extraLarge = 0;
  let colossal = 0;

  for (let { properties } of features) {
    const val = properties[filteredParamName];
    if (val >= 0 && val < 15) {
      small++;
    } else if (val >= 15 && val < 30) {
      middle++;
    } else if (val >= 30 && val < 50) {
      large++;
    } else if (val >= 70 && val < 200) {
      extraLarge++;
    } else if (val >= 200) {
      colossal++;
    }
  }

  tempResult = {
    "0-15": small,
    "15-30": middle,
    "30-50": large,
    "70-200": extraLarge,
    "200+": colossal,
  };

  return tempResult;
};

export { filterDataBySOCstock };

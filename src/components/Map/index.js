import React, { useState, useRef, useEffect } from "react";
import ReactMapGL from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { COLOR_PALETTES, MAPBOX } from "constants.js";
import { makeStyles } from "@material-ui/core/styles";
import "mapbox-gl/dist/mapbox-gl.css";
import * as turf from "@turf/turf";
import { useSelector, useDispatch } from "react-redux";
import { fetchRacaData, setDisplayedData } from "redux/actions";
import _ from "lodash";
import mapboxgl from "mapbox-gl";
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker';

const useStyles = makeStyles(() => ({
  mapWrapper: {
    height: "100%",
    width: "100%",
    position: "fixed",
    top: 0,
    left: 0,
  },
}));

function Map() {
  let _mapRef = useRef(null);
  const classes = useStyles();
  const dispatch = useDispatch();

  const [isMapReady, setMapReady] = useState(false);
  const [viewport, setViewport] = useState({ ...MAPBOX.VIEWPORT });
  const [bounds, setBounds] = useState(null);

  const racaData = useSelector((state) => state.map.racaData);
  const colorPallete = useSelector((state) => state.map.colorPallete);
  const filteredParam = useSelector((state) => state.map.filteredParam);

  // @ts-ignore
  mapboxgl.workerClass =
    require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default;

  useEffect(() => {
    dispatch(fetchRacaData());
  }, []);

  useEffect(() => {
    if (_mapRef.current.getMap().getLayer("raca")) {
      _mapRef.current.getMap().setPaintProperty("raca", "circle-color", {
        property: filteredParam,
        stops: [
          [15, COLOR_PALETTES[filteredParam][0]],
          [30, COLOR_PALETTES[filteredParam][1]],
          [50, COLOR_PALETTES[filteredParam][2]],
          [70, COLOR_PALETTES[filteredParam][3]],
          [200, COLOR_PALETTES[filteredParam][4]],
        ],
      });
    }
  }, [colorPallete, filteredParam]);

  if (isMapReady) {
    if (!_mapRef.current.getMap().getSource("raca")) {
      _mapRef.current
        .getMap()
        .addSource("raca", { type: "geojson", data: racaData });

      _mapRef.current.getMap().addLayer({
        id: "raca",
        type: "circle",
        source: "raca",
        layout: {},
        paint: {
          "circle-radius": 10,
          "circle-color": {
            property: "SOCstock30",
            stops: [
              [15, "#5F6B9D"],
              [30, "#385A83"],
              [50, "#6F85AC"],
              [70, "#9CA1B0"],
              [200, "#C87D6A"],
            ],
          },
        },
      });
    }
  }

  const handleInteraction = (interactionState) => {
    // Check if the user has stopped interacting with the map (e.g. panning/dragging/zooming)
    if (
      interactionState.isDragging ||
      interactionState.inTransition ||
      interactionState.isRotating ||
      interactionState.isZooming ||
      interactionState.isHovering ||
      interactionState.isPanning
    ) {
      return;
    }

    isMapReady && setBounds(_mapRef.current.getMap().getBounds());

    if (bounds && racaData) {
      // Get map extent bounds (bbox sw and ne points), and form a polygon out of that
      const swPoint = turf.point([bounds._sw.lng, bounds._sw.lat]);
      const nePoint = turf.point([bounds._ne.lng, bounds._ne.lat]);
      const currentMapBboxPolygon = turf.bboxPolygon(
        turf.bbox(turf.featureCollection([swPoint, nePoint]))
      );

      // Check if the point inside of the bounding box
      let filtered = racaData.features.filter((feature) => {
        return turf.inside(
          turf.point(feature.geometry.coordinates),
          currentMapBboxPolygon
        );
      });

      dispatch(
        setDisplayedData({
          type: "FeatureCollection",
          totalFeatures: filtered.length,
          features: filtered,
        })
      );
    }
  };

  return (
    <div className={classes.mapWrapper}>
      <ReactMapGL
        ref={_mapRef}
        {...viewport}
        onLoad={() => setMapReady(true)}
        width="100%"
        height="100%"
        onViewportChange={(vp) => setViewport(vp)}
        onInteractionStateChange={(s) => handleInteraction(s)}
        mapboxApiAccessToken={MAPBOX.API_TOKEN}
        mapStyle={MAPBOX.VIEWPORT.mapStyle}
      ></ReactMapGL>
    </div>
  );
}

export default Map;

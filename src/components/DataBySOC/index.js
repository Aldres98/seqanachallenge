import { useEffect } from "react";
import Chart from "../Chart";
import { useSelector, useDispatch } from "react-redux";
import { setDataInViewportBySOC } from "redux/actions";
import { filterDataBySOCstock } from "utils/chartTools";

const DataBySOC = () => {
  const dispatch = useDispatch();
  const displayedData = useSelector((state) => state.map.displayedData);
  const filteredParam = useSelector((state) => state.map.filteredParam);
  const dataBySOC = useSelector((state) => state.charts.dataBySOC);

  useEffect(() => {
    if (Object.entries(displayedData).length) {
      dispatch(setDataInViewportBySOC(filterDataBySOCstock(displayedData.features, filteredParam)));
    } else {
      dispatch(setDataInViewportBySOC(null));
    }
  }, [displayedData, filteredParam]);


  return (
    dataBySOC && (
      <Chart
        labels={Object.keys(dataBySOC)}
        values={Object.values(dataBySOC)}
        title={`Features in viewport by ${filteredParam}`}
      />
    )
  );
};

export default DataBySOC;

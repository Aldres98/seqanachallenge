import { Pie } from "react-chartjs-2";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import { COLOR_PALETTES } from "../../constants";
const useStyles = makeStyles((theme) => ({
  chartTitle: {
    textAlign: "center",
    WebkitTextStroke: "1px black"
  },
}));

const Chart = (props) => {
  const classes = useStyles();
  const filteredParam = useSelector((state) => state.map.filteredParam);

  const data = {
    labels: props.labels,
    datasets: [
      {
        data: props.values,
        backgroundColor: COLOR_PALETTES[filteredParam],
        borderColor: [
          "rgba(255, 255, 255, 1)"
        ],
        borderWidth: 1,
      },
    ],
  };

  return (
    <>
      <h2 className={classes.chartTitle}>{props.title}</h2>
      <Pie
        data={data}
        onElementsClick={(elems) => props.onClick(elems)}
      />
    </>
  );
};

Chart.propTypes = {
  labels: PropTypes.array.isRequired,
  values: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
};

export default Chart;

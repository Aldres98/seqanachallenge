import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import { useDispatch } from "react-redux";
import { setFilteredParam } from "redux/actions";

const FilterButton = (props) => {
  const { filteredParamName } = props;
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(
        setFilteredParam(filteredParamName)
      );

  }

  return <Button variant="contained" onClick={handleClick} style={{marginTop: 10}}>Color by {filteredParamName}</Button>;
};

FilterButton.propTypes = {
  filteredParamName: PropTypes.string.isRequired,
};

export default FilterButton;

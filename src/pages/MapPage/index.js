import React from "react";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Map from "components/Map";
import DataBySOC from "components/DataBySOC";
import FilterButton from "components/FilterButton";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "none!important",
    width: "100%",
    height: "100vh",
    padding: 0,
  },
  timeline: {
    position: "fixed",
    bottom: "60px",
    left: 0,
    width: "95%",
    margin: "0 2.5%",
    fontSize: "12px",
    transition: ".5s",
  },
  filtersText: {
    textAlign: "center",
  },
}));

function MapPage() {
  const classes = useStyles();

  return (
    <Grid container className={classes.root}>
      <Map />
      <div
        style={{
          display: "flex",
          position: "fixed",
          flexDirection: "column",
          justifyContent: "space-between",
          top: "15px",
          width: 350,
          height: 150,
          right: "15px",
          color: "#fff",
        }}
      >
        <DataBySOC />
        <FilterButton filteredParamName="SOCstock5" />
        <FilterButton filteredParamName="SOCstock30" />
        <FilterButton filteredParamName="SOCstock100" />
      </div>
    </Grid>
  );
}

export default MapPage;

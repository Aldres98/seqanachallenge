export const MAPBOX = {
  API_TOKEN:
    'pk.eyJ1IjoiZG9uaW5wciIsImEiOiJjaXVxM3E4OTkwMDAwMm9wczBnYjR4bnJoIn0.7drt92qBRl7KJ6dLg0mrww',
  VIEWPORT: {
    latitude: 44,
    longitude: -95,
    zoom: 3,
  },
};


export const COLOR_PALETTES = {
  SOCstock5: ['#C7E8F3', '#BF9ACA', '#8E4162', '#41393E', '#EDA2C0'],
  SOCstock30: ['#5F6B9D', '#385A83', '#6F85AC', '#9CA1B0', '#C87D6A'],
  SOCstock100: ['#26547C', '#EF476F', '#FFD166', '#06D6A0', '#FFFCF9'],

}
import { MAP_READY, START_FETCHING_DATA, GET_DATA, SET_VIEWPORT_FEATURES, RESTORE_DEFAULT_VIEW, SET_FILTERED_PARAM, SET_COLOR_PALETTE, SET_DISPLAYED_DATA } from "../actionTypes";
import { COLOR_PALETTES } from "../../constants";

const initialState = {
  isMapReady: false,
  isDataLoading: false,
  racaData: {},
  displayedData: {},
  viewportFeatures: {},
  filteredParam: "SOCstock30",
  colorPallete: COLOR_PALETTES["SOCstock30"]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case MAP_READY: {
      return {
        ...state,
        isMapReady: true,
      };
    }

    case SET_FILTERED_PARAM: {
      return {
        ...state,
        filteredParam: action.payload
      }
    }

    case SET_COLOR_PALETTE: {
      return {
        ...state,
        colorPallete: action.payload
      }
    }

    case START_FETCHING_DATA: {
      return {
        ...state,
        isDataLoading: true,
      };
    }

    case GET_DATA: {
      return {
        ...state,
        racaData: action.payload,
        displayedData: action.payload,
        isDataLoading: false,
      };
    }

    case SET_VIEWPORT_FEATURES: {
      return {
        ...state,
        viewportFeatures: action.payload
      }
    }

    case SET_DISPLAYED_DATA: {
      return {
        ...state,
        displayedData: action.payload
      }
    }

    case RESTORE_DEFAULT_VIEW: {
      return {
        ...state,
        displayedData: state.racaData,
        materialFilter: null,
        sizeFilter: null
      }
    }
    default:
      return state;
  }
}

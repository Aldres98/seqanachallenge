import { combineReducers } from 'redux';

import map from './map';
import charts from './charts'

export default combineReducers({
  map,
  charts
});

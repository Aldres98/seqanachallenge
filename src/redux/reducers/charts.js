import { SET_DATA_IN_VIEWPORT_BY_SOCSTOCK } from "../actionTypes";

const initialState = {
  dataBySOC: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_DATA_IN_VIEWPORT_BY_SOCSTOCK: {
      return {
        ...state,
        dataBySOC: action.payload,
      };
    }

    default:
      return state;
  }
}

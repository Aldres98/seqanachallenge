import {
  MAP_READY,
  START_FETCHING_DATA,
  GET_DATA,
  SET_DATA_IN_VIEWPORT_BY_SOCSTOCK,
  SET_VIEWPORT_FEATURES,
  RESTORE_DEFAULT_VIEW,
  SET_COLOR_PALETTE,
  SET_FILTERED_PARAM,
  SET_DISPLAYED_DATA,
} from "./actionTypes";

export const mapReady = () => ({
  type: MAP_READY,
  payload: {},
});

export const startFetchingData = () => ({
  type: START_FETCHING_DATA,
  payload: {},
});

export const getData = (payload) => ({
  type: GET_DATA,
  payload: payload,
});

export const setColorPaletter = (payload) => ({
  type: SET_COLOR_PALETTE,
  payload: payload
})

export const setFilteredParam = (payload) => ({
  type: SET_FILTERED_PARAM,
  payload: payload
})

export const setDisplayedData = (payload) => ({
  type: SET_DISPLAYED_DATA,
  payload: payload
})

export const setViewportFeatures = (payload) => ({
  type: SET_VIEWPORT_FEATURES,
  payload: payload,
});


export const restoreDefaultView = () => ({
  type: RESTORE_DEFAULT_VIEW,
  payload: {}
})

export const setDataInViewportBySOC = (payload) => ({
  type: SET_DATA_IN_VIEWPORT_BY_SOCSTOCK,
  payload: payload,
});

export const fetchRacaData = () => {
  return (dispatch) => {
    dispatch(startFetchingData());
    fetch("/raca.geojson")
      .then((resp) => {
        return resp.json();
      })
      .then((data) => {
        dispatch(getData(data));
        dispatch(setViewportFeatures(data.features));
      });
  };
};

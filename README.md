# Seqana challenge

Data visualisation for Seqana. I tried to make the visualisation a little bit more interesting, by displaying interactive pie chart below the map legend, which shows ammount of features by selected SOCstock in the map viewport (currently visible features - I tried to make it more or less good in performance, therefore all filtering is applied only when user stopped all interaction with a map itself (pan/drag/zoom etc), this is done using Mapbox's standart events, although could've done the same with debounce). You can also display features by SOCstock5, SOCstock30 and SOCstock100 by clicking the corresponding button. 

The challenge was done with React + Redux + Mapbox as those technologies are the most convenient for me to work with and suit the task. Although, I would love to discuss a couple of other options that could be used in case of a bigger ammount of data points (e.g., deckGL, Tile server e.t.c.)

To run the application: 
 `yarn install`
 `yarn start`
 
Alternatively, go to the build folder and run index.html 
 
I've also set up a CI/CD pipeline, which builds the app, puts it into S3 storage and invalidates cloudfront cache in order to host it online. You can find the app here: https://d16yyqut9y6t5o.cloudfront.net/

Stuff done with the data: 
1) Download the data
2) Merge CSV's using QGIS by the propper id field
3) Check if there are any duplicates or points with missing data (there were some points with missing data) 
4) Check, if the chosen coordinate system (WGS84) suits the data provided by displaying data points in QGIS
5) Export as GEOJSON

The challenge took roughly 7 hours :)
